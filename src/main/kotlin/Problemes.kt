class Problemes {
    var problemes = mutableListOf<Problema>()

    init{
        problemes = BaseDades().getProblems()
    }

    fun solveds(no:Boolean):List<Problema>{
        val resolts = mutableListOf<Problema>()
        val noResolts = mutableListOf<Problema>()
        for(problema in this.problemes){
            if(problema.resolt) resolts.add(problema)
            else noResolts.add(problema)
        }
        return if(no) noResolts
        else resolts
    }

    fun showProblems(tema:String){
        val problemesTema = mutableListOf<Problema>()
        for(problema in this.problemes){
            if(problema.tema == tema) problemesTema.add(problema)
        }
        var i = 1
        for (problema in problemesTema) {
            problema.printProblem(i)
            i++
        }
        do {
            println("Vols resoldre algun problema?")
            println("En cas afirmatiu escriu-ne el seu número,")
            print("en qualsevol altre cas escriu 'no': ")
            var respostaUsuari = escaner.next()
            println()
            if(respostaUsuari == "no") problemsList()
            else if(respostaUsuari.toInt() in 1 .. problemesTema.size){
                problemesTema[respostaUsuari.toInt()-1].solveProblem()
            }else respostaUsuari = "error"
        }while (respostaUsuari == "error")
    }
}

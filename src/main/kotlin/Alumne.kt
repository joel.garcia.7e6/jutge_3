class Alumne {
    val problemesAlumne = Problemes()

    fun continueItin(){
        val problemes = this.problemesAlumne.solveds(true)
        var i = 0
        var seguir = true
        do{
            problemes[i].printProblem(i+1)
            println("Vols resoldre'l?")
            println("     1. Si     2. No     3. Tornar al menú")
            do{
                print("Escull una opció vàlida:   ")
                var respostaUsuari = escaner.next()
                when(respostaUsuari){
                    "1" -> {
                        println()
                        problemes[i].solveProblem()
                        i++
                    }
                    "2" -> {
                        println()
                        i++
                    }
                    "3" -> {
                        seguir=false
                    }
                    else -> respostaUsuari = "error"
                }
            }while (respostaUsuari=="error")
        }while (i in problemes.indices&&seguir)
    }

    fun historialProblems(){
        val problemes = this.problemesAlumne.solveds(false)
        for (problema in problemes) problema.printStatus()
        studentMenu()
    }
}

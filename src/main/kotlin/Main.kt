import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.Scanner
import kotlin.system.exitProcess

val escaner = Scanner(System.`in`)

const val contrasenyaProfessor = "ITB2023"

lateinit var alumne: Alumne

var connection: Connection? = null

fun main(){
    val jdbcUrl = "jdbc:postgresql://localhost:5432/jutgitb"
    try {
        connection = DriverManager.getConnection(jdbcUrl)
        if(File("./problemes.json").exists()){
            val problemesJSON = File("./problemes.json").readText()
            val problemes = Json.decodeFromString<List<Problema>>(problemesJSON)
            for(problema in problemes){
                BaseDades().addProblem(problema)
            }
            File("./problemes.json").delete()
        }
        println("Benvingut a JutgITB")
        println()
        choseRol()
    } catch (e: SQLException) {
        println("JutgITB ha experimentat un error intern: ${e.errorCode} ${e.message}")
    }
}

fun choseRol(){
    println("Identifica't:")
    println("     1- Sóc alumne     2- Sóc professor")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                alumne = Alumne()
                studentMenu()
            }
            "2" -> {
                println()
                profesor()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
}

fun profesor(){
    print("Introdueix contrasenya: ")
    val contrasenyaUser = escaner.next()
    if(contrasenyaProfessor == contrasenyaUser) {
        println()
        profesorMenu()
    }
    else {
        println()
        choseRol()
    }
}

fun studentMenu(){
    println("MENÚ:")
    println("     1- Seguir amb l’itinerari d’aprenentatge")
    println("     2- Llista problemes")
    println("     3- Consultar històric de problemes resolts")
    println("     4- Ajuda")
    println("     5- Sortir")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                alumne.continueItin()
            }
            "2" -> {
                println()
                problemsList()
            }
            "3" -> {
                println()
                alumne.historialProblems()
            }
            "4" -> {
                println()
                help()
            }
            "5" -> {
                println()
                println("Fins aviat!")
                exitProcess(0)
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
    println()
    studentMenu()
}

fun problemsList(){
    var tema = ""
    println("  Llista de problemes:")
    println("     1- Tipus de dades")
    println("     2- Condicionals")
    println("     3- Bucles")
    println("     4- Llistes")
    println("     5- Tornar al menú")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                tema = "Tipus de dades"
            }
            "2" -> {
                println()
                tema = "Condicionals"
            }
            "3" -> {
                println()
                tema = "Bucles"
            }
            "4" -> {
                println()
                tema = "Llistes"
            }
            "5" -> {
                println()
                studentMenu()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
    Problemes().showProblems(tema)
    problemsList()
    println()
}

fun help() {
    println("JutgITB és el software més complert i fet servir arreu per resoldre i corregir problemes. Gràcies a la seva simplicitat")
    println("única l'alumne pot gaudir d'una experiència educativa sense precedents.")
    println("El sistema de resolució de problemes és molt senzill. Per cada problema plantejat l'alumne pot decidir si resoldre'l o no.")
    println("En alguns casos es passarà al problema següent i en alguns d'altres l'alumne tindra accés de nou al menú per decidir què")
    println("vol treballar. Al final de cada problema resolt es mostra el recull dels intents que l'alumne ha emprat per arribar a la")
    println("solució.")
    println("En general JutgITB és una aplicació molt intuitiva, encarada precisament a donar una experiència còmode tant a alumnes com a")
    println("professors.")
    println("Bona feina!")
}

fun profesorMenu(){
    println("MENÚ:")
    println("     1- Afegir nous problemes")
    println("     2- Treure un report de la feina de l'alumne")
    println("     3- Sortir")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                addProblem()
                println()
                profesorMenu()
            }
            "2" -> {
                println()
                studentSolved()
            }
            "3" -> {
                println()
                println("Fins aviat!")
                exitProcess(0)
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
}

fun addProblem(){
    val atributsProblemaNom = listOf("tema(\"Tipus de dades\", \"Condicionals\", \"Bucles\" i \"Llistes\")","títol","enunciat","input públic","output públic","input privat","output privat")
    val atributsProblemaValor = mutableListOf<String>()
    for(atribut in atributsProblemaNom){
        print("Introdueix $atribut: ")
        val inputUsuari = escaner.next() + escaner.nextLine()
        atributsProblemaValor.add(inputUsuari)
    }
    val problema = Problema(0,atributsProblemaValor[0],atributsProblemaValor[1],atributsProblemaValor[2],atributsProblemaValor[3],atributsProblemaValor[4],atributsProblemaValor[5],atributsProblemaValor[6])
    BaseDades().addProblem(problema)
}

fun studentSolved(){
    val problemesResolts = Problemes().solveds(false)
    val problemesResoltsSize = problemesResolts.size
    val problemesPossibles = Problemes().problemes.size
    val intentsDescomptar = discountTry(problemesResolts)
    val notaSobreDeu = problemesResoltsSize.toDouble()/problemesPossibles.toDouble()*10.0
    println("  Dades a reportar:")
    println("     1- Treure una puntuació en funció dels problemes resolts")
    println("     2- Descomptar per intents")
    println("     3- Mostrar-ho de manera gràfica")
    println("     4- Tornar al menu")
    do{
        print("Escull una opció vàlida: ")
        var opcioUsuari = escaner.next()
        when(opcioUsuari){
            "1" -> {
                println()
                println("$notaSobreDeu/10")
            }
            "2" -> {
                println()
                println("${notaSobreDeu-(0.1*intentsDescomptar)}/10")
            }
            "3" -> {
                println()
                showGraphic(Problemes().problemes)

            }
            "4" -> {
                println()
                profesorMenu()
            }
            else -> opcioUsuari = "error"
        }
    }while (opcioUsuari == "error")
    println()
    studentSolved()
}

fun discountTry(problemes: List<Problema>):Int{
    var intentsTotals = 0
    for(problema in problemes){
        if(problema.intentsDescomptar>1)intentsTotals+=problema.intentsDescomptar
    }
    return intentsTotals
}

fun showGraphic(problemes: List<Problema>){
    val intentMesLlarg = longerTry(problemes)
    val titolMesLlarg = longerTitle(problemes)
    for(problema in problemes){
        print("${spaces(titolMesLlarg,problema.titol)}${problema.titol}: ")
        for(intent in problema.intents_usuari){
            print("${spaces(intentMesLlarg,intent)}$intent | ")
        }
        println()
    }
}

fun longerTry(problemes: List<Problema>):Int{
    var intentMesLlarg = 0
    for(problema in problemes){
        for(intent in problema.intents_usuari){
            if(intent.length>intentMesLlarg) intentMesLlarg = intent.length
        }
    }
    return intentMesLlarg
}

fun longerTitle(problemes: List<Problema>):Int{
    var titolMesLlarg = 0
    for(problema in problemes){
        if(problema.titol.length>titolMesLlarg)titolMesLlarg=problema.titol.length
    }
    return titolMesLlarg
}

fun spaces(caractersTotals:Int, caractersConcrets:String):String{
    var espais = ""
    for(i in 1..caractersTotals-caractersConcrets.length) espais+=" "
    return espais
}

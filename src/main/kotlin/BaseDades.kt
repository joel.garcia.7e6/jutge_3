import java.sql.SQLException

class BaseDades {
    fun addProblem(problema: Problema) {
        val query = connection!!.prepareStatement("INSERT INTO problemes (tema, titol, enunciat, input_public, output_public, input_privat, output_privat, resolt, intentsdescomptar, intentstotals)" +
                "VALUES (?,?,?,?,?,?,?,?,?,?)")
        try {
            query.setString(1, problema.tema)
            query.setString(2, problema.titol)
            query.setString(3, problema.enunciat)
            query.setString(4, problema.input_public)
            query.setString(5, problema.output_public)
            query.setString(6, problema.input_privat)
            query.setString(7, problema.output_privat)
            query.setBoolean(8, problema.resolt)
            query.setInt(9, problema.intentsDescomptar)
            query.setInt(10, problema.intentsTotals)
            query.executeUpdate()
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al afegir el problema: ${e.message}")
        }
    }

    fun getProblems():MutableList<Problema>{
        val problemes = mutableListOf<Problema>()
        val query = connection!!.createStatement().executeQuery("SELECT * FROM problemes")

        try {
            while(query.next()){
                val id_problema = query.getInt("id_problema")
                val tema = query.getString("tema")
                val titol = query.getString("titol")
                val enunciat = query.getString("enunciat")
                val input_public = query.getString("input_public")
                val output_public = query.getString("output_public")
                val input_privat = query.getString("input_privat")
                val output_privat = query.getString("output_privat")
                val problema = Problema(
                    id_problema,
                    tema,
                    titol,
                    enunciat,
                    input_public,
                    output_public,
                    input_privat,
                    output_privat
                )
                problema.intents_usuari = this.getTry(problema.id_problema)
                problemes.add(problema)
            }
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al rebre els problemes: ${e.message}")
        }

        return problemes
    }

    fun addTry(idProblema:Int, intent:String){
        try {
            connection!!.createStatement().executeUpdate("INSERT INTO intents_usuari (id_problema,intent) VALUES ($idProblema,$intent)")
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al afegir l'intent: ${e.message}")
        }
    }

    fun getTry(idProblema: Int):MutableList<String>{
        val intents = mutableListOf<String>()
        val query = connection!!.createStatement().executeQuery("SELECT * FROM intents_usuari WHERE id_problema = $idProblema")
        try{
            while(query.next()){
                val intent = query.getString("intent")
                intents.add(intent)
            }
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al rebre els intets: ${e.message}")
        }
        return intents
    }

    fun updateProblems(problema: Problema){
        val update = "UPDATE problemes SET tema = ?, titol = ?, enunciat = ?,input_public = ?, output_public = ?,input_privat = ?,output_privat = ?,resolt = ?,intentsDescomptar = ?,intentsTotals = ? WHERE id_problema = ?"
        val query = connection!!.prepareStatement(update)
        try{
            query.setString(1, problema.tema)
            query.setString(2, problema.titol)
            query.setString(3, problema.enunciat)
            query.setString(4, problema.input_public)
            query.setString(5, problema.output_public)
            query.setString(6, problema.input_privat)
            query.setString(7, problema.output_privat)
            query.setBoolean(8, problema.resolt)
            query.setInt(9, problema.intentsDescomptar)
            query.setInt(10, problema.intentsTotals)
            query.setInt(11,problema.id_problema)
            query.executeUpdate()
            query.close()
        }catch (e: SQLException) {
            println("Error ${e.errorCode} al actualitzar el problema: ${e.message}")
        }
    }
}
